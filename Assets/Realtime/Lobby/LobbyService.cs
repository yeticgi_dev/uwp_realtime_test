using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Realtime.Ortc;
using Realtime.Ortc.Api;
using UnityEngine;
using Realtime.Demos;

namespace Realtime.Lobby
{
	public class demoData
	{
		public string demo;
		public List<string> buttons;
	}
    /// <summary>
    /// Wraps the ortc client to provide common game-play functionality such as found in uNet
    /// </summary>
	public class LobbyService
    {

        #region subs
        protected const string LOBBY = "lobby";
        protected const string OrtcDisconnected = "ortcClientDisconnected";
        protected const string OrtcConnected = "ortcClientConnected";
        protected const string OrtcSubscribed = "ortcClientSubscribed";
        protected const string OrtcUnsubscribed = "ortcClientUnsubscribed";
        protected const string Seperator = ";;";

        /// <summary>
        /// Internal announcment scheme
        /// </summary>
        protected class OrtcAnnouncement
        {
            /// <summary>
            /// User Metadata
            /// </summary>
            public string cm;
        }

        public enum ConnectionState
        {
            Disconnected,
            Connecting,
            Connected,
            Reconnecting,
            Disconnecting
        }
        #endregion

        #region events
        /// <summary>
        /// Connection state. eg : connected, reconnecting
        /// </summary>
        public event Action<ConnectionState> OnState = delegate { };


        #endregion

        #region properties
        /// <summary>
        /// Current connection state
        /// </summary>
        public ConnectionState State { get; private set; }

        /// <summary>
        /// Self
        /// </summary>
        public UserDetails User { get; private set; }


        #endregion

        #region methods

        #region init

        /// <summary>
        /// Static instance
        /// </summary>
        public static LobbyService Instance { get; set; }

        /// <summary>
        /// Initializer
        /// </summary>
        /// <param name="client"></param>
        /// <param name="appKey"></param>
        /// <param name="privateKey"></param>
        /// <param name="url"></param>
        /// <param name="isCluster"></param>
        /// <returns></returns>
        public static LobbyService Init(IOrtcClient client, string appKey, string privateKey, string url, bool isCluster, LobbyTest lobbyTest)
        {
            //TODO Unity 5.2 and below, add your json serializer here
#if UNITY_4_7
            Debug.LogWarning("Please Add a JsonSerializer Here.");
#else
            _toJson = JsonUtility.ToJson;
            _fromJson = JsonUtility.FromJson;
#endif

            Instance = new LobbyService(client, appKey, privateKey, url, isCluster, lobbyTest);
            return Instance;
        }

        /// <summary>
        /// Connect
        /// </summary>
        public void Connect(string authToken, UserDetails self, Action<ConnectionState> callback)
        {
            if (State == ConnectionState.Connected)
            {
                Debug.LogWarning("Already Connected !");
                callback(State);
                return;
            }

            AuthKey = authToken;
            User = self;
            connectCallback = callback;

            _client.ConnectionMetadata = User.UserId;

            State = ConnectionState.Connecting;
            OnState(State);

            EnablePresence(LOBBY);

            _client.Connect(AppKey, AuthKey);
        }

        public void Disconnect()
        {
            if (State == ConnectionState.Disconnected)
            {
                Debug.LogError("Already Disconnected !");
                return;
            }

            

            State = ConnectionState.Disconnecting;
            OnState(State);

            _client.Disconnect();
        }

#endregion



#region sending




#endregion

#region receiving

        /// <summary>
        /// Add a listener
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="callback"></param>
        public void Subscribe<T>(LobbyMessenger<T>.LobbyMessageDelegate callback) where T : LobbyMessage
        {
            LobbyMessenger<T>.Subscribe(callback);
        }

        /// <summary>
        /// Remove a listener
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="callback"></param>
        public void Unsubscribe<T>(LobbyMessenger<T>.LobbyMessageDelegate callback) where T : LobbyMessage
        {
            LobbyMessenger<T>.Unsubscribe(callback);
        }

#endregion

#endregion

#region internal

        IOrtcClient _client;

        public string AuthKey { get; set; }
        public string AppKey { get; set; }
        public string PrivateKey { get; set; }
        public string Url { get; set; }
        public bool IsCluster { get; set; }
        public object Terminal { get; private set; }


        Action<ConnectionState> connectCallback;

        static Func<string, Type, object> _fromJson;
        static Func<object, string> _toJson;

        public LobbyTest lobbyTest;

        LobbyService(IOrtcClient client, string appKey, string privateKey, string url, bool isCluster, LobbyTest lobbyTest)
        {
            this.lobbyTest = lobbyTest;
            AppKey = appKey;
            PrivateKey = privateKey;
            Url = url;
            IsCluster = isCluster;
            
            _client = client;

            if (isCluster)
            {
                _client.ClusterUrl = url;
            }
            else
            {
                _client.Url = url;
            }

            _client.OnException += _client_OnException;
            _client.OnConnected += _client_OnConnected;
            _client.OnDisconnected += _client_OnDisconnected;
            _client.OnReconnected += _client_OnReconnected;
            _client.OnReconnecting += _client_OnReconnecting;
            _client.OnSubscribed += _client_OnSubscribed;
            _client.OnUnsubscribed += _client_OnUnsubscribed;


        }

      

        string ToJson(object o)
        {
            return _toJson(o);
        }

        object FromJson(string json, Type t)
        {
            return _fromJson(json, t);
        }    
  
        T FromJson<T>(string json) where T : class 
        {
            return _fromJson(json, typeof(T)) as T;
        }

        void EnablePresence(string channel)
        {
            PresenceClient.EnablePresence(Url, IsCluster, AppKey, PrivateKey, channel, true,
                (ex, r) =>
                {
                    if (ex == null)
                    {
                        Debug.Log("Presence Enabled : " + channel);
                    }
                    else
                    {
                        Debug.LogError("Presence Error : " + ex.Message);
                    }
                });
        }

       public static void sendMessage(String channel, String message)
       {
            Instance._client.Send(channel, message);
       }

        void _client_OnUnsubscribed(string channel)
        {
            
        }

        void _client_OnSubscribed(string channel)
        {
            if (channel == User.UserId)
            {
                State = ConnectionState.Connected;
                OnState(State);

                if (connectCallback != null)
                    connectCallback(ConnectionState.Connected);

                connectCallback = null;
            }

			if (channel == "lobby") 
			{
				sendMessage ("lobby", "channelNumber: " + lobbyTest.channelNumber.ToString ());
			}
			if (channel.Contains ("immyDemo"))
			{
				var obj = generateDemoData();

				sendMessage ("immyDemo" + lobbyTest.channelNumber.ToString(), _toJson(obj));
			}
            
        }

		demoData generateDemoData()
		{
			var data = new demoData
			{
				demo = lobbyTest.demoName,
				buttons = new List<string>()
			};
			if (lobbyTest.demoName == "distance") 
			{
                data.buttons.Add("showDistance");
				data.buttons.Add("text:move out,command:moveOut");
				data.buttons.Add("text:move in,command:moveIn");
			}
            if (lobbyTest.demoName == "text")
            {
                data.buttons.Add("showDistance");
                data.buttons.Add("text:move out,command:moveOut");
                data.buttons.Add("text:move in,command:moveIn");
                data.buttons.Add("test:Enlarge Text, command:enlargeText");
                data.buttons.Add("test:Shrink Text, command:shrinkText");
                data.buttons.Add("test:Toggle Clear, command:toggleClear");
                data.buttons.Add("test:Toggle Color, command:toggleColor");
                
            }

            if (lobbyTest.demoName == "dino") 
			{
                data.buttons.Add("text:Reset Reference,command:resetReference");
				data.buttons.Add("text:Toggle Skybox,command:toggleSkybox");
                data.buttons.Add("text:Pause,command:togglePause");
            }

			if (lobbyTest.demoName == "turtle") 
			{
                data.buttons.Add("text:Reset Reference,command:resetReference");
                data.buttons.Add("text:Toggle Skybox,command:toggleSkybox");
				data.buttons.Add("text:Pause,command:togglePause");
			}
            data.buttons.Add("test:Toggle RoomNum, command:toggleRoomNum");
            data.buttons.Add("text:Toggle Mask,command:toggleMask");

            if (lobbyTest.demoName != "distance") data.buttons.Add ("text:Distance Demo,command:changeDemo_distance");
			if(lobbyTest.demoName != "dino") data.buttons.Add ("text:Dino Demo,command:changeDemo_dino");
            if (lobbyTest.demoName != "turtle") data.buttons.Add("text:Turtle Demo,command:changeDemo_turtle");
            if (lobbyTest.demoName != "text") data.buttons.Add("text:Text Demo,command:changeDemo_text");

            data.buttons.Add("text:Exit,command:exit");


            return data;
		}

        void _client_OnReconnecting()
        {
            State = ConnectionState.Reconnecting;
            OnState(State);
        }

        void _client_OnReconnected()
        {

            State = ConnectionState.Connected;
            OnState(State);
        }

        void _client_OnDisconnected()
        {
            

            State = ConnectionState.Disconnected;
            OnState(State);

            if (connectCallback != null)
                connectCallback(ConnectionState.Disconnected);

            connectCallback = null;

        }

        void _client_OnConnected()
        {
            _client.Subscribe(OrtcDisconnected, true, OnOrtcMessage);
			_client.Subscribe(User.UserId, true, OnOrtcMessage);
			//_client.Subscribe("lobby", true, OnOrtcMessage);
        }

        void _client_OnException(Exception ex)
        {
            Debug.LogException(ex);


            

            connectCallback = null;
            
        }

        //message comes in here
        void OnOrtcMessage(string channel, string message)
        {
            
            
			if (channel == OrtcDisconnected) 
			{
				var model = FromJson<OrtcAnnouncement> (message);

				//Send via messenger. Routed below
				LobbyMessenger.Publish (channel, model, typeof(OrtcAnnouncement));
			}
            //our messages go here
			else if (channel == "immyDemo" + lobbyTest.channelNumber) 
			{
				if (message == "requestDemo") 
				{
					var obj = generateDemoData ();

					sendMessage ("immyDemo" + lobbyTest.channelNumber.ToString (), _toJson (obj));
				}
				lobbyTest.PassMessageUp (message);

			} 
			else if (channel == "lobby") 
			{
				Debug.Log ("new available channel :: " + message);
			}
            

        }

#endregion


    }
	
	
}