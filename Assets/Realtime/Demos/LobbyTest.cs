// -------------------------------------
//  Domain		: IBT / Realtime.co
//  Author		: Nicholas Ventimiglia
//  Product		: Messaging and Storage
//  Published	: 2014
//  -------------------------------------
using System;
using System.Linq;
using Foundation.Debuging;
using Realtime.Lobby;
using Realtime.Ortc;
using Realtime.Ortc.Api;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using Immy.Tracking.Trackers;

namespace Realtime.Demos
{
    /// <summary>
    /// Demo Client using the Ortc CLient
    /// </summary>
    [AddComponentMenu("Realtime/Demos/LobbyTest")]
    public class LobbyTest : MonoBehaviour
    {

        public GameObject controller;
        //public string controlerName;
		public string demoName = "";
		public int channelNumber = 0;
        /// <summary>
        /// 
        /// </summary>
        public string URL = "https://ortc-developers.realtime.co/server/2.1";

        /// <summary>
        /// Identities your channel group
        /// </summary>
        public string ApplicationKey = "";

        /// <summary>
        /// Identities your channel group
        /// </summary>
        public string PrivateKey = "";

        public string messageString = "Hello";

        private IOrtcClient _ortc;
        private LobbyService _lobby;

        void Awake()
        {
			System.Random r = new System.Random ();
            if (channelNumber == 0)
            {
                channelNumber = r.Next(1, 999);
            }

            //GameObject.Find("channelNumText").GetComponent<Text>().text = channelNumber.ToString();

			SceneManager.sceneLoaded += onSceneLoaded;
            RealtimeProxy.ConfirmInit();
            _ortc = new UnityOrtcClient();
            _ortc.ClusterUrl = URL;
            _lobby = LobbyService.Init(_ortc, ApplicationKey, PrivateKey, URL, true,this);

            _lobby.OnState += _lobby_OnState;
			DontDestroyOnLoad (GameObject.Find("BBRemote"));
			//DontDestroyOnLoad (_lobby);

        }

        public void PassMessageUp(string message)
        {
            //Debug.Log("Passed up To Lobby Test");
            if(demoName == "text")
            {
                //controller.transform.GetComponent<TextDemoRealTimeController>().getMessage(message,channelNumber.ToString());
            }
            if(demoName == "dino")
            {
                //controller.transform.GetComponent<DinoDemoRealTimeController>().getMessage(message, channelNumber.ToString());
            }
            if (demoName == "turtle")
            {
                //controller.transform.GetComponent<TurtleDemoRealTimeController>().getMessage(message, channelNumber.ToString());
            }
            if(demoName == "distance")
            {
                //controller.transform.GetComponent<DistanceDemoRealTimeController>().getMessage(message, channelNumber.ToString());
            }
            /*if (message == "moveIn") 
			{
				GameObject ball = GameObject.Find ("bask");
				ball.GetComponent<CamTraslator> ().pullBall ();
				LobbyService.sendMessage ("immyDemo" + channelNumber.ToString(), "Distance:" + ball.transform.localPosition.z.ToString ());
			} 
			else if (message == "moveOut") 
			{
                Debug.Log("moveOut");
                GameObject ball = GameObject.Find ("bask");
				ball.GetComponent<CamTraslator> ().pushBall ();
				LobbyService.sendMessage ("immyDemo" + channelNumber.ToString(), "Distance:" + ball.transform.localPosition.z.ToString ());
			} */
            if (message.StartsWith ("changeDemo")) 
			{
				
				string newDemo = message.Split ("_" [0]) [1];
				Debug.Log (newDemo);
				changeScenes (newDemo);
			} 
            /*
			else if (message == "toggleSkybox") 
			{
				
				GameObject.Find ("Toggler").GetComponent<Toggle> ().toggleState ();
			}
			else if (message == "togglePause") 
			{
                if(demoName == "dino")
                    GameObject.Find("Toggler").GetComponent<Pauser_Dino_360>().setPause();
                else
                    GameObject.Find ("Toggler").GetComponent<Pauser> ().setPause();
			}
            else if(message == "toggleMask")
            {
                GameObject maskImage = GameObject.Find("HUD").transform.FindChild("MaskImage").gameObject;
                maskImage.SetActive(!maskImage.activeSelf);
            }
            else if(message == "resetReference")
            {
                GameObject.Find("Stereo Camera").GetComponent<FreeSpaceTracker>().ResetReference();
            }
			else
			{
				Debug.Log("recieved a differesnt message " + message);
            }*/
            

        }



		public void changeScenes(string sceneName)
		{
			
			if (sceneName == demoName)
				return;
			demoName = sceneName;
			Disconnect ();
			SceneManager.LoadScene (sceneName);


		}

		void onSceneLoaded(Scene scene, LoadSceneMode mode)
		{
            demoName = scene.name;
            controller = GameObject.Find(demoName + "Controller");
            Connect();
            if (demoName == "Launcher") changeScenes("turtle");
            //GameObject.Find("channelNumText").GetComponent<Text>().text = channelNumber.ToString();
            //if(demoName != "Launcher")GameObject.Find("channelNumText").GetComponent<TextMeshPro>().text = channelNumber.ToString();
            
        }

        private void _lobby_OnState(LobbyService.ConnectionState obj)
        {
            if (obj != LobbyService.ConnectionState.Connected)
                Terminal.LogImportant(obj);
        }



//        protected void Start()
//        {
//            Connect();
//        }

        #region methods

        void Connect()
        {
            var user = new UserDetails
            {
				UserId = "immyDemo" + channelNumber.ToString(),
				UserName = "immyDemo" + channelNumber.ToString(),
            };

            _lobby.Connect("AuthKey", user, state =>
            {
                Terminal.LogImportant("Connected !");
            });
        }

        void Disconnect()
        {
            _lobby.Disconnect();
        }

       

        #endregion
    }


}
