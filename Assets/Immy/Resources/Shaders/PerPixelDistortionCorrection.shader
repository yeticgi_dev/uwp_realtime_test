﻿Shader "Hidden/Immy/PerPixelDistortionCorrection" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "" {}
	}
	Subshader{
		Pass{
			CGPROGRAM
				#pragma vertex vertex_shader
				#pragma fragment fragment_shader

				sampler2D _MainTex;
				float distortion_coefficients[15];
				sampler2D distortion_mask;

				struct vertex_input {
					float4 position : POSITION;
					float4 uv_coordinates : TEXCOORD0;
				};

				struct vertex_output {
					float4 screen_position : SV_POSITION;
					float2 clip_coordinates : TEXCOORD1;
					float2 uv_coordinates : TEXCOORD0;
				};

				float2 CorrectDistortion(float2 clip_coordinates) {
					return float2(
						distortion_coefficients[0] * clip_coordinates.x * pow(clip_coordinates.y, 3) +
						distortion_coefficients[1] * pow(clip_coordinates.x, 3) * clip_coordinates.y +
						distortion_coefficients[2] * clip_coordinates.x * pow(clip_coordinates.y, 2) +
						distortion_coefficients[3] * pow(clip_coordinates.x, 3) +
						distortion_coefficients[4] * clip_coordinates.x * clip_coordinates.y +
						distortion_coefficients[5] * clip_coordinates.x,

						distortion_coefficients[6] * pow(clip_coordinates.y, 4) +
						distortion_coefficients[7] * pow(clip_coordinates.x, 2) * pow(clip_coordinates.y, 2) +
						distortion_coefficients[8] * pow(clip_coordinates.x, 4) +
						distortion_coefficients[9] * pow(clip_coordinates.y, 3) +
						distortion_coefficients[10] * pow(clip_coordinates.x, 2) * clip_coordinates.y +
						distortion_coefficients[11] * pow(clip_coordinates.y, 2) +
						distortion_coefficients[12] * pow(clip_coordinates.x, 2) +
						distortion_coefficients[13] * clip_coordinates.y +
						distortion_coefficients[14]
					);
				}

				float2 ClipToUV(float2 clip_coordinates ) {
					return float2( 0.5 * (clip_coordinates.x + 1.0), 0.5 * (clip_coordinates.y + 1.0) );
				}

				vertex_output vertex_shader( vertex_input input ) {
					vertex_output output;

					output.screen_position = mul( UNITY_MATRIX_MVP, input.position );
					output.clip_coordinates = output.screen_position.xy;
					output.uv_coordinates = input.uv_coordinates;

					return output;
				}

				float4 fragment_shader(vertex_output input) : SV_Target{
					float4 color = tex2D( _MainTex, ClipToUV( CorrectDistortion( input.clip_coordinates) ) );
					color *= tex2D(distortion_mask, input.uv_coordinates);
					return color;
				}
			ENDCG
		}
	}
	Fallback off
}
