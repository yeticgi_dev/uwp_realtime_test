﻿Shader "Hidden/Immy/UIComposite" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "" {}
	}
	Subshader{
		Pass{
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
				#pragma vertex vertex_shader
				#pragma fragment fragment_shader

				sampler2D _MainTex;
				struct vertex_input {
					float4 position : POSITION;
					float4 uv_coordinates : TEXCOORD0;
				};

				struct vertex_output {
					float4 screen_position : SV_POSITION;
					float2 uv_coordinates : TEXCOORD0;
				};

				vertex_output vertex_shader(vertex_input input) {
					vertex_output output;
					output.screen_position = mul(UNITY_MATRIX_MVP, input.position);
					output.uv_coordinates = float2(input.uv_coordinates.x, input.uv_coordinates.y);

					return output;
				}

				float4 fragment_shader(vertex_output input) : SV_Target{
					return tex2D(_MainTex, input.uv_coordinates);
				}
			ENDCG
		}
	}
	Fallback off
}
